<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserIdInPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photos', function (Blueprint $table) {
            $table->string('path')->after('id');
            $table->integer('imageable_id')->after('path');
            $table->integer('imageable_type')->after('imageable_id');
            $table->dropColumn(['$user_id', 'file_name', 'description', 'image', 'file_type', 'file_size', 'file_mime']);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function (Blueprint $table) { 
        });
    }
}
