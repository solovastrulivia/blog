-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2019 at 11:18 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

CREATE TABLE `objects` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `description` text NOT NULL,
  `price` float UNSIGNED DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `objects`
--

INSERT INTO `objects` (`id`, `title`, `description`, `price`, `updated_at`, `created_at`) VALUES
(1, 'Object 1', 'Description 1', 20, '2019-06-13 11:07:44', '2019-06-13 11:07:44'),
(2, 'Object 2', 'Description 2', 10, '2019-06-22 21:04:01', '2019-06-13 11:28:55'),
(6, 'Object 3', 'Description 3', 3, '2019-07-04 14:10:48', '2019-07-04 14:10:48'),
(7, 'Object 4', 'Description 4', 4, '2019-07-04 14:11:17', '2019-07-04 14:11:17');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `description` text NOT NULL,
  `price` float UNSIGNED DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `price`, `updated_at`, `created_at`) VALUES
(1, 'Product 1', 'Description 1', 23, '2019-07-07 09:06:29', '2019-06-13 11:07:44'),
(2, 'Product 2', 'Description 2', 10, '2019-06-22 21:04:01', '2019-06-13 11:28:55'),
(16, 'Product 3', 'Description 3', 3, '2019-06-28 13:35:43', '2019-06-21 10:31:16'),
(22, 'Product 5', 'Description  5', 5, '2019-06-28 13:36:50', '2019-06-22 21:08:21'),
(23, 'Product 6', 'Description 6', 6, '2019-06-28 13:38:07', '2019-06-23 13:46:39'),
(24, 'Product 7', 'Description 77', 7, '2019-06-28 18:10:19', '2019-06-26 16:14:45'),
(29, 'Product 8', 'Description 8', 8, '2019-06-28 13:38:39', '2019-06-26 16:27:54');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `description` text NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `description`, `updated_at`, `created_at`) VALUES
(6, 'Project 1', 'Description descrpted 1', '2019-06-20 18:35:16', '2019-06-15 08:21:53'),
(7, 'Project 2', 'Description 2', '2019-06-15 08:25:55', '2019-06-15 08:25:55'),
(8, 'Project 45', 'Desc 45', '2019-06-15 09:52:48', '2019-06-15 08:59:42'),
(9, 'Project 5', 'Description 5', '2019-06-15 09:01:59', '2019-06-15 09:01:59'),
(100000, 'Project 8', 'Description desc', '2019-07-25 14:03:42', '2019-06-15 09:03:19'),
(100001, 'Project 10', 'Desc 10', '2019-06-15 09:23:51', '2019-06-15 09:23:51');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `project_id` int(11) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `completed` int(1) NOT NULL DEFAULT '0',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `objects`
--
ALTER TABLE `objects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100002;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
