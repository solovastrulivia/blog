<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Object extends Model
{
	protected $table='objects';
    protected $fillable = ['title', 'description', 'price'];
    //public $timestamps=false;    
}