<?php

namespace App\Http\Controllers;

use App\Project;

class ProjectsController extends Controller 
{
	public function add()
	{
		$project = new Project;

		$project->title = 'Project 2';
		$project->description = 'Description 2';

		$project->save();

		return $project; 
	}

	public function index()
	{
		
		$projects = \App\Project::all();

		//return \App\Project::all()[0]->title;
		//return \App\Project::all()->map->title;
		//return $projects;

		return view('projects.index', [
			'projects' => $projects
		]);
	}

	public function show(Project $project)
	{
		//$project = Project::findOrFail($id);
		//dd($project->tasks);

		return view('projects.show', compact('project'));
	}

	public function create()
	{
		return view('projects.create');
	}

	public function edit(Project $project)
	{
		//dd($project);
		return view('projects.edit', compact('project'));
	}

	public function update(Project $project)
	{
		//dd(request()->all());
		
		request()->validate([
			'title' => 'required',
			'description' => 'required'
		]);

		$project->title = request('title');
		$project->description = request('description');

		$project->save();

		return redirect('projects');
	}

	public function store()
	{
		//return request()->all();
		//return request('title');

		// v4
		Project::create(request()->validate([
			'title' => 'required',
			'description' => 'required'
		]));

		// v3
		/*$attributes = request()->validate([
			'title' => 'required',
			'description' => 'required'
		], [
			'title.required' => 'The title is needed'
		]);

		Project::create($attributes);*/


		// v2
		/*request()->validate([
			'title' => 'required',
			'description' => 'required'
		], [
			'title.required' => 'The title is needed'
		]);
		
		// v1
		Project::create(request()->all());*/
		/*$project = new Project;

		$project->title = request('title');
		$project->description = request('description');

		$project->save();*/

		return redirect('/projects');

	}

	public function destroy(Project $project)
	{
		//Project::find($id)->delete();
		$project->delete();
		
		return redirect('/projects');
	}
	
}