<?php

namespace App\Http\Controllers;

class PagesController extends Controller 
{
	public function index()
	{
		$tasks = [
			'Go to school',
			'Learn'
		];

		return view('welcome', [
			'tasks' => $tasks,
			'title' => 'Welcome'
		]);

		//return view('welcome', compact('tasks'));
	}

	public function about()
	{
		return view('about');
	}

	public function contact()
	{
		return view('contact');
	}

	public function termsAndConditions()
    {
        return view('pages.terms_and_conditions');
    }

    public function showVlayer()
    {
        return view('pages.vlayer');
    }
}