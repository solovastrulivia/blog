<?php

namespace App\Http\Controllers;

use App\Task;
use App\Project;

class ProjectTasksController extends Controller
{
	public function show($id)
	{
		$task = Task::findOrFail($id);

		dd($task->project);
	}

	public function update(Task $task)
	{
		$task->update(['completed' => request()->has('completed')]);

		return back();
	}

	public function store(Project $project)
	{
		$attributes = request()->validate([
			'description' => 'required'
		]);
		// v1
		/*Task::create([
			'project_id' => $project->id,
			'description' => request('description')
		]);*/

		// v2
		$project->addTask($attributes);

		return back();
	}
}