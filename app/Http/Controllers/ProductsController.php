<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Requests;
use App\Product;
use Request;
use Session;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        //dd($products);
        return view('products.show', [
            'products' => $products
        ]);
    }

    public function viewAll()
    {
        $products = Product::all();

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.addproduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        // validate input data
        Product::create(request()->all());
        $product = new Product;
        //dd($product);
        $product->title = request('title');
        $product->description = request('description');
        $product->price = request('price');
        //$product->save();

        Session::flash('message','Record stored!');
        return redirect('/products/index');

        // dd($_POST);die;
        // if(!Product::isValid(Request::all())){
        //     return redirect()->back()->withInput()->withErrors(Product::$errors);
        // }

        // $product = new Product;
        // $product->title = Request::input('title');
        // $product->description = Request::input('description');
        // $product->price = Request::input('price');
        // $product->save();

        // Session::flash('message','Record stored!');
        // return redirect('products/index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
        $product=Product::where('title','=',$title)->first();
        return view('products.showproduct',['product'=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $product=Product::where('id','=',$id)->first();
        return view('products.editproduct',['product'=>$product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        //Product $product
        //dd(request()->all());
        // $attributes = request()->validate([
        //     'title' => 'required',
        //     'description' => 'required',
        //     'price' => 'required'
        // ], [
        //     'title.required' => 'The title is needed'
        // ]);
        // Product::create($attributes);

        // request()->validate([
        //     'title' => 'required',
        //     'description' => 'required',
        //     'price' => 'required'
        // ], [
        //     'title.required' => 'The title is needed'
        // ]);
        
        // request()->validate([
        //     'title' => 'required',
        //     'description' => 'required',
        //     'price' => 'required'
        // ]);

        // $product->title = request('title');
        // $product->description = request('description');
        // $product->price = request('price');
        // $product->save();

        // Product::create(request()->all());
        // $product = new Product;
        // $product->title = request('title');
        // $product->description = request('description');
        // $product->price = request('price');
        //$product->save()  

        //;

        //Session::flash('message','Record updated!');
        //return redirect('products/index');

        if(!Product::isValid(Request::all())){
            return 'Data entered is not good. Check again!';
        }else{
            Product::where('id','=',$id)->update(array(
                'title'=>Request::input('title'),
                'description'=>Request::input('description'),
                'price'=>Request::input('price'),
            ));

            Session::flash('message','Record updated!');
            return redirect('/products/index');
        }
    }


    /**
     * Remove the specified resource from stora


                ge.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $product=Product::where('id','=',$id)->first();
        if($product->delete()){
            $products = Product::all();
            Session::flash('message','Record was deleted!');
            return redirect('/products/index');
        }else{
            Session::flash('message','Error! Please try again!');
            return redirect('/products/index');
        }
    }
}
