<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Obbject;
use Session;

class ObjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = Obbject::all();
        return view('objects.index', compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('objects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Obbject::create(request()->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]));

        Session::flash('message','Record stored!');
        return redirect('/objects');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Obbject $object)
    {
        //dd($object);
        //$object = Obbject::findOrFail($id);

        return view('objects.show', compact('object'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Obbject $object)
    {
        return view('objects.edit', compact('object'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Obbject $object)
    {
        //dd(request()->all());
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]);

        $object->title = request('title');
        $object->description = request('description');
        $object->price = request('price');
        $object->save();

        Session::flash('message','Record updated!');
        return redirect('/objects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Obbject $object)
    {
        dd($object);
        //Obbject::find($id)->delete();
        if($object->delete()){
            $objects =Obbject::all();
            Session::flash('message','Record deleted!');
            return redirect('/objects');
        }else{
            Session::flash('message','Error! Please try again!');
            return redirect('/objects');
        }
    }
}
