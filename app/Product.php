<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'price'];
    //public $timestamps=false;
    protected $table='products';

    public static $errors;
    public static $rules = [
        'title'=>'required',
        'description'=>'required',
        'price'=>'required',
    ];

    public static function isValid($data){
        $validation=Validator::make($data,static::$rules);
        if($validation->passes()) return true;
        static::$errors=$validation->messages();
        return false;
    }

    public function Photos(){
        return $this->morphMany('App\Photo', 'Imageable');
    }
}