<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = ['name'];

    public function Photos(){
        return $this->morphMany('App\Photo', 'Imageable');
    }
}
