@extends('objects.layout')


@section('header')
Webshop
@endsection
@section('content')

		<div class="container">
		<div class="jumbotron">
			<h1 class="p-2 text-primary">Details for: {{$object->title}}</h1>
			<br>
			<h3 class="p-2">Description: {{$object->description}}</h3>
			<h3 class="p-2">Price: {{$object->price}}</h3>
			<br>
			<span class="p-3">
			{!!Html::link("/objects",'Back', ['class' => 'btn btn-primary'])!!}</span>
			<!-- <a href='http://localhost/larafacultate/public/flowers'>Back</a> -->
		</div>
	</div>
	
@endsection