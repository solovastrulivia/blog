@extends('objects.layout')


@section('header')
Webshop
@endsection
@section('content')

  <div class = "container bg-light p-5 my-5 border border-info rounded-sm">
    <nav class="nav nav-pills flex-column flex-sm-row mt-5">
      <a class="flex-sm-fill text-sm-center nav-link active" href="/blog/public/objects">Objects</a>
      <a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/objects/all">All</a>
      <a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/objects/create">Add</a>
      <a class="flex-sm-fill text-sm-center nav-link disabled" href="/blog/public/objects/{$object->id}/edit" tabindex="-1" aria-disabled="true">Edit</a>
      <a class="flex-sm-fill text-sm-center nav-link disabled" href="/blog/public/objects/delete/{$object->id}" tabindex="-1" aria-disabled="true">Delete</a>
    </nav>    
    <br>
    <hr>

    @if(count($objects)==0)
    <div class="d-flex justify-content-center">No objects in database!</div>
    @else
    <table class="container table table-striped table-hover border border-info  text-center py-4 mt-5">
      <tr class="bg bg-primary text-white">
        <th>Title</th>
        <th>Description</th>
        <th>Price</th>
        <th>Actions</th>
      </tr>
      @foreach($objects as $object)
      <tr>
        <td>{{$object->title}}</td>
        <td>{{$object->description}}</td>
        <td>{{$object->price}}</td>
        </div>
        <td>
          <div class = "row">
            <div class="col-sm">
            {!!Html::link("/objects/{$object->id}",'View',['class' => 'btn btn-primary'])!!} <!-- <a href="{!!url('objects/{$object->title}')!!}" class="btn btn-xs btn-dark pull-right">View</a> -->
            </div>
            <div class="col-sm">
            {!!Html::link("/objects/{$object->id}/edit",'Edit',['class' => 'btn btn-primary'])!!}</div>
            
              <!-- form -method = post action objects/{$object->id} -->
              <div class="col-sm">
              <form method="post" action="objects/<?php echo $object->id;?>">
                <?php echo method_field('Delete');?>
                <?php echo csrf_field(); ?>
                <button type="submit" class ='btn btn-primary'>Delete</button>
              </form>
            </div>
            </div>
            </td>
          </tr>
          @endforeach
        </table>
        @endif

        <br/>
        <div class="container d-flex justify-content-center">
        {!!Html::link("/objects/create",'Add object',['class' => 'btn btn-primary'])!!}</div>
        <br/><br/>
        <div class="d-flex justify-content-center">
          @if(Session::has('message'))
          {{Session::get('message')}}
          @endif
        </div>
      </div>
@endsection