@extends('objects.layout')

@section('header')
Webshop
@endsection
@section('content')

<div class = "container bg-light p-5 my-5 border border-info rounded-sm">
	<nav class="nav nav-pills flex-column flex-sm-row mt-5">
		<a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/objects">Objects</a>
		<a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/objects/all">All</a>
		<a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/objects/add">Add</a>
		<a class="flex-sm-fill text-sm-center nav-link  active" href="/blog/public/objects/edit/{$object->id}">Edit</a>
		<a class="flex-sm-fill text-sm-center nav-link disabled" href="/blog/public/objects/delete/{$object->id}" tabindex="-1" aria-disabled="true">Delete</a>
	</nav>
	<br>
	<hr>

	<div class="d-flex justify-content-center p-5">
		<div class="card bg-light" style="width: 30rem;">
			<div class="card-title">
				<h1 class="text-primary p-3 pl-5">Update object</h1>
			</div>
			<div class="card-body">
				{!!Form::model($object, array('url'=>'/objects/'.$object->id))!!}
				{{Form::hidden('id')}} 
				@csrf
				
				{{ method_field('PATCH') }}
				
				<div class="form-group row">
					<div class="col-4">
						{!!Form::label('title','Title:')!!}
					</div>
					<div class="col-8">
						{!!Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'required'])!!}
						{!!$errors->first('title')!!}
					</div>
				</div><br/>
				<div class="form-group row">
					<div class="col-4">
						{!!Form::label('description','Description:')!!}
					</div>
					<div class="col-8">
						{!!Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'description', 'rows' => 4, 'cols' => 54, 'style' => 'resize:none', 'required'])!!}
						{!!$errors->first('description')!!}
					</div>
				</div><br/>
				<div class="form-group row">
					<div class="col-4">
						{!!Form::label('price','Price:')!!}
					</div>
					<div class="col-8">
						{!!Form::text('price', old('price'), ['class' => 'form-control', 'id' => 'price', 'required'])!!}
						{!!$errors->first('price')!!}
					</div>
				</div><br/>
				<div class="form-group row">
					<div class="col-12 d-flex justify-content-center">
						{{Form::hidden('id')}}
						{!!Form::submit('Update object', ['class' => 'btn btn-primary'])!!}
					</div>
				</div>
				{!!Form::close()!!}
			</div>
		</div>
	</div>
</div>

@endsection