@extends('products.master')

@section('content')

<br>
<hr>
<br>

<h1 class="d-flex justify-content-center py-4 text-primary"> All products</h1>
<div class='row'>
@foreach($products as $product)
<div class="col-6">
	<div class="jumbotron p-4 m-3 border border-primary">
	<div class='row p-1'>
		<div class='col-4'>Title:</div>
    	<div class='col-8'>{{$product->title}}</div>
	</div>
	<div class='row p-1'>
    	<div class='col-4'>Description:</div>
    	<div class='col-8'>{{$product->description}}</div>
    </div>
    <div class='row p-1'>
    	<div class='col-4'>Price:</div>
    	<div class='col-8'>{{$product->price}}</div>
	</div>
</div>
</div>
@endforeach
</div>
@endsection
