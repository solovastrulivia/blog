
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Products</title>
</head>
<body>
  <div class = "container bg-light p-5 my-5 border border-info rounded-sm">
    <div class="justify-content-center display-4 mb-5">Webshop</div>
  </div>
  <div class = "container bg-light p-5 my-5 border border-info rounded-sm">
    <nav class="nav nav-pills flex-column flex-sm-row mt-5">
      <a class="flex-sm-fill text-sm-center nav-link active" href="/blog/public/products/index">Products</a>
      <a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/products/all">All</a>
      <a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/products/add">Add</a>
      <a class="flex-sm-fill text-sm-center nav-link disabled" href="/blog/public/products/edit/{$product->id}" tabindex="-1" aria-disabled="true">Edit</a>
      <a class="flex-sm-fill text-sm-center nav-link disabled" href="/blog/public/products/delete/{$product->id}" tabindex="-1" aria-disabled="true">Delete</a>
    </nav>
    
    <br>
    <hr>

    @if(count($products)==0)
    <div class="d-flex justify-content-left">No products in database!</div>
    @else
    <table class="container table table-striped table-hover border border-info  text-center py-4 mt-5">
      <tr class="bg bg-primary text-white">
        <th>Title</th>
        <th>Description</th>
        <th>Price</th>
        <th>Actions</th>
      </tr>
      @foreach($products as $product)
      <tr>
        <td>{{$product->title}}</td>
        <td>{{$product->description}}</td>
        <td>{{$product->price}}</td>
        <td>
          <div class="d-flex justify-content-center">
            <div class="row d-flex justify-content-center">
              <div class="col p-2">
                <!-- {!!Html::link("/products/{$product->title}",'View',['class' => 'btn btn-primary'])!!} --> 
                <!-- <a href="{!!url('products/{$product->title}')!!}" class="btn btn-xs btn-dark pull-right">View</a> -->
                <form action="/blog/public/products/<?php echo $product->title; ?>">
                  <button type="submit"> View </button>
                </form> 
              </div>
              <div class="col p-2">
                <!-- {!!Html::link("/products/edit/{$product->id}",'Edit',['class' => 'btn btn-primary'])!!} -->
                <form action="/blog/public/products/edit/<?php echo $product->id; ?>">
                  <button type="submit"> Edit </button>
                </form>
                <!-- <button type="submit" formaction="URL"> -->
                </div>
                <div class="col p-2">
                  <!-- form -method = post action products/delete/{$product->id} -->
                  <!-- {!!Html::link("/products/delete/{$product->id}",'Delete',['class' => 'btn btn-primary'])!!} -->
                  <form action="delete/<?php echo $product->id; ?>">
                    <button type="submit"> Delete </button>
                  </form>
                </div>
              </div>
            </div>
          </td>
        </tr>
        @endforeach
      </table>
      @endif

      <br/>
      <div class="container d-flex justify-content-center">
        <!-- {!!Html::link("/products/add",'Add product',['class' => 'btn btn-primary'])!!} -->
        <form action="/blog/public/products/create">
          <button type="submit">Add button</button>
        </form>
      </div>
      <br/><br/>
      <div class="d-flex justify-content-center">
        @if(Session::has('message'))
        {{Session::get('message')}}
        @endif
      </div>
    </div>
  </body>
  </html>