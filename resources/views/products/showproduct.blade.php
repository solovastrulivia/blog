<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<head><title>Product</title></head>
	<body>
		<div class = "container bg-light p-5 my-5 border border-info rounded-sm">
			<div class="d-flex justify-content-center display-4 mb-5">Webshop</div>
		</div>
		<div class="container">
		<div class="jumbotron">
			<h1 class="p-2 text-primary">Details for: {{$product->title}}</h1>
			<br>
			<h3 class="p-2">Description: {{$product->description}}</h3>
			<h3 class="p-2">Price: {{$product->price}}</h3>

			<span class="p-2">
			{!!Html::link("/products/index",'Back', ['class' => 'btn btn-primary'])!!}</span>
			<!-- <a href='http://localhost/larafacultate/public/flowers'>Back</a> -->
		</div>
	</div>
	</body>
	</html>