<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<head><title>Update product</title></head>
	<body>
		<div class = "container bg-light p-5 my-5 border border-info rounded-sm">
			<div class="d-flex justify-content-center display-4 mb-5">Webshop</div>
		</div>
		<div class = "container bg-light p-5 my-5 border border-info rounded-sm">
			<nav class="nav nav-pills flex-column flex-sm-row mt-5">
				<a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/products/index">Products</a>
				<a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/products/all">All</a>
				<a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/products/add">Add</a>
				<a class="flex-sm-fill text-sm-center nav-link  active" href="/blog/public/products/edit/{$product->id}">Edit</a>
				<a class="flex-sm-fill text-sm-center nav-link disabled" href="/blog/public/products/delete/{$product->id}" tabindex="-1" aria-disabled="true">Delete</a>
			</nav>

			<br>
			<hr>
			
			<div class="d-flex justify-content-center p-5">
				<div class="card bg-light" style="width: 24rem;">
					<div class="card-title">
						<h1 class="text-primary p-3 pl-5">Update product</h1>
					</div>
					<div class="card-body">
						{!!Form::model($product,array('url'=>'/products/update/'.$product->id))!!}
						{{Form::hidden('id')}} 
						<div class="form-group row">
							<div class="col-4">
								{!!Form::label('title','Title:')!!}
							</div>
							<div class="col-8">
								{!!Form::text('title', null, ['class' => 'form-control', 'id' => 'title'])!!}
							</div>
						</div><br/>
						<div class="form-group row">
							<div class="col-4">
								{!!Form::label('description','Description:')!!}
							</div>
							<div class="col-8">
								{!!Form::text('description', null, ['class' => 'form-control', 'id' => 'description'])!!}
							</div>
						</div><br/>
						<div class="form-group row">
							<div class="col-4">
								{!!Form::label('price','Price:')!!}
							</div>
							<div class="col-8">
								{!!Form::text('price', null, ['class' => 'form-control', 'id' => 'price'])!!}
							</div>
						</div><br/>
						<div class="form-group row">
							<div class="col-12 d-flex justify-content-center">

								{!!Form::submit('Update product', ['class' => 'btn btn-primary'])!!}
							</div>
						</div>
						{!!Form::close()!!}
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>