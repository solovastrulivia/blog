@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="recaptcha" class="col-md-4 col-form-label text-md-right">{{ __('Captcha') }}</label>

                            <div class="col-md-6">
                                @if( config('app.google_recaptcha_key') )
                                    <div class="g-recaptcha" data-sitekey="{{ config('app.google_recaptcha_key')}}"></div>
                                    @error('g-recaptcha-response')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="form-check">
                                        <div class="row">
                                            <div class="col-1 pr-5">
                                                <input class="form-check-input"  type="checkbox" id="acceptConditions" name="acceptConditions" class="form-control require-one @error('acceptConditions') is-invalid @enderror">
                                                @error('acceptConditions')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-11 ml-1">
                                                <label class="form-check-label" for="check">
                                                <a href="#terms-and-conditions" data-toggle="modal" data-remote="{{ route('pages.termsAndConditions') }}" data-target="#terms-and-conditions">{{ __('app.i_accept_terms_and_conditions') }}</a>
                                                <!-- button type="button" class="btn btn-primary" data-toggle="modal" data-target="#terms-and-conditions">{ __('app.i_accept_terms_and_conditions') }}</button> -->
                                                </label>
                                            </div>
                                        </div>
                                        <div class="error_terms"></div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="terms-and-conditions" tabindex="-1" role="dialog" aria-labelledby="modal-1" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-1">{{ __('app.terms_and_conditions')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm " data-dismiss="modal">{{ __('app.close')}}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-js')
<script>
    $( function() {
        $("#users_register").validate({
            rules: {
                'name' : {
                    required: true,
                    minlength: 3
                },
                'email': {
                    required: true,
                    email: true
                },
                'password': {
                    required: true,
                    minlength: 8
                },
                'password_confirmation': {
                    required: true,
                    equalTo: "#password",
                    minlength: 8
                },
                'acceptConditions':{

                     required: true //{
                    //     depends: function(element) {
                    //         return $('.require-one:checked').size() == 0;
                    //     }
                    // }
                },
                'g-recaptcha-response':{

                    required: true
                }
            },
            messages:{

                'name' : {
                    required: "{{ __('validation.please_add_name') }}",
                    minlength: "{{ __('validation.please_add_min3_characters') }}"
                },
                'email' : {
                    required: "{{ __('validation.please_add_email') }}",
                    email: "{{ __('validation.please_add_valid_email') }}",
                    //unique: "Va rugam introduceti o alta adresa de email"
                },
                'password': {
                    required: "{{ __('validation.please_add_password') }}",
                    minlength: "{{ __('validation.please_add_min8_characters') }}"
                },
                'password_confirmation': {
                    required: "{{ __('validation.please_confirm_password') }}",
                    equalTo: "{{ __('validation.password_invalid') }}",
                    minlength: "{{ __('validation.please_add_min8_characters') }}"
                },
                'acceptConditions':{

                    required: "{{ __('validation.confirm_read_conditions') }}",
                },
                'g-recaptcha-response':{

                    required: "{{ __('validation.confirm_not_robot') }}",
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "acceptConditions" )
                    error.insertAfter(".error_terms");
                else
                    error.insertAfter(element);
            }
        
        });


        $('#terms-and-conditions').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var modal = $(this);

            modal.find('.modal-body').load(button.data("remote"));
        });





    });
</script>

@endsection