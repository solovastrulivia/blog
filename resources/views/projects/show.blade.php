@extends('layout')

@section('content')
<h1><?php echo $project->title;?></h1>
<div><?php echo $project->description;?></div>
<?php //if ($project->tasks->count()):?>
<ul>
<?php 
/*foreach ($project->tasks as $key => $value) {
	echo '<li>
	<form method="POST" action="/tasks/'.$value->id.'">
	'.csrf_field().method_field('PATCH').'
	<input type="checkbox" name="completed" value="1" onchange="this.form.submit()" '.($value->completed?'checked':'').'> '.$value->description.'
	</form>
	</li>';
}*/
?>
</ul>
<?php //endif; ?>
<a href="<?php echo $project->id;?>/edit">Edit</a>

<br /><br />
<form method="POST" action="{{ $project->id }}/tasks">
@csrf
<h2>Add a task</h2>
	<div>
		<input type="text" name="description" class="<?php echo $errors->has('description')?'error':'';?>" placeholder="Place a description" value="<?php echo old('description');?>">
	</div>
	<div>
		<button type="submit">Submit</button>
	</div>
	<?php 
	if ($errors->any()):
	?>
	<div style="background:red;color:white;padding:10px;">
	<?php
		foreach ($errors->all() as $error) {
			echo $error.'<br />';
		}
	?>
	</div>
	<?php endif;?>
</form>
@endsection
