@extends('layout')

@section('content')
<h1>Edit a project</h1>
<form method="POST" action="/blog/public/projects/<?php echo $project->id;?>">
	<?php 
	echo csrf_field();
	echo method_field('PATCH');
	?>
	<div>
		<input type="text" name="title" placeholder="Place a title" value="<?php echo $project->title;?>">
	</div>
	<div>
		<textarea name="description" placeholder="Place a description"><?php echo $project->description;?></textarea>
	</div>
	<div>
		<button type="submit">Update project</button> 
	</div>
	@include('errors')
</form>
<form method="POST" action="projects/<?php echo $project->id;?>">
	<?php echo method_field('Delete');?>
	<?php echo csrf_field(); ?>
	<button type="submit">Delete project</button>
</form>
@endsection
