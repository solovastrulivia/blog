@extends('layout')

@section('content')
<h1>Create a new project</h1>
<form method="post" action="projects">
	{{ csrf_field() }}
	<div>
		<input type="text" name="title" class="<?php echo $errors->has('title')?'error':'';?>" placeholder="Place a title" value="<?php echo old('title');?>">
	</div>
		<textarea class="<?php echo $errors->has('description')?'error':'';?>" name="description" placeholder="Place a description"><?php echo old('description');?></textarea>
	</div>
	<div>
		<button type="submit">Submit</button>
	</div>
	@include('errors')
</form>
@endsection