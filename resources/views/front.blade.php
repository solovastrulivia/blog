<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title class="d-flex ">
        	@yield('title')
        </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        
    </head>
    <body>
    	<div class="container">
    		<h1><?php echo $title; ?></h1>
    <h1>{{ $title }}</h1>
    <ul>
    <?php 
    foreach ($tasks as $key => $value) {
        echo '<li>'.$value.'</li>';
    }
    ?>
    </ul>
    	</div>

    	@yield('footer')
    </body>
</html>