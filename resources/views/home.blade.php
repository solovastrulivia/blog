@extends('layouts.app')

@section('content')
  <div class = "container bg-light p-5 my-5 border border-info rounded-sm">
      <div class="d-flex justify-content-center display-4 mb-5">My Blog</div>
  </div>
  <div class = "container bg-light p-5 my-5 border border-info rounded-sm">
    <h2 class="d-flex justify-content-center py-4 text-primary"> Welcome on the site!</h2>
    <h5 class="d-flex justify-content-center py-4 text-primary"> Here you can choose some options</h5>
    <br>
    <nav class="nav nav-pills flex-column flex-sm-row mt-5">
      <a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/users">Users</a>
      <a class="flex-sm-fill text-sm-center nav-link active" href="/blog/public/home">Home</a>
      <a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/gallery">Gallery</a>
    </nav>
    <nav class="nav nav-pills flex-column flex-sm-row mt-5">
      <a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/products/index">Products</a>
      <a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/objects">Objects</a>
      <a class="flex-sm-fill text-sm-center nav-link" href="/blog/public/projects">Projects</a>
    </nav>
  </div>
@endsection
<!-- </body>
</html>
 -->