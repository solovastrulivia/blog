<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', function () {
    return view('home');
});

Route::get('/', 'PagesController@index');
Route::get('about', 'PagesController@about');
Route::get('contact', 'PagesController@contact');

//Route::get('/add', 'ProjectsController@add');
//Route::get('/projects/add', 'ProjectsController@add');

Route::get('projects', 'ProjectsController@index');
Route::post('projects', 'ProjectsController@store');

Route::prefix('projects')->group(function () {
	Route::get('/create', 'ProjectsController@create');
	Route::get('/{project}', 'ProjectsController@show');
	Route::get('/{project}/edit/', 'ProjectsController@edit');
	Route::patch('/{project}', 'ProjectsController@update');
	Route::delete('/{projects}', 'ProjectsController@destroy');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

//Route::resource('projects', 'ProjectsController');

/*Route::get('/projects/add', 'ProjectsController@add');
Route::get('/projects', 'ProjectsController@index');
Route::get('/projects/{project}', 'ProjectsController@show');
Route::get('/projects/create', 'ProjectsController@create');
Route::post('/projects', 'ProjectsController@store');
Route::get('/projects/{project}/edit', 'ProjectsController@edit');
Route::patch('/projects/{project}', 'ProjectsController@update');
Route::delete('/projects/{projects}', 'ProjectsController@destroy');
*/

Route::get('/blog/public/projects/tasks/{task}', 'ProjectTasksController@show');
Route::patch('/blog/public/projects/tasks/{task}', 'ProjectTasksController@update');

// add a task to a project
Route::post('/blog/public/projects/{project}/tasks', 'ProjectTasksController@store');

Route::prefix('products')->group(function () {
	Route::get('/all', 'ProductsController@viewAll');
	Route::get('/index', 'ProductsController@index');
	Route::get('/add', 'ProductsController@create');
	Route::post('/store', 'ProductsController@store');
	Route::get('/{title}','ProductsController@show');
	Route::get('/edit/{id}','ProductsController@edit');
	Route::post('/update/{id}','ProductsController@update');
	Route::get('/delete/{id}','ProductsController@destroy');
});

Route::resource('objects', 'ObjectsController');

Route::prefix('objects')->group(function () {
	Route::get('/', 'ObjectsController@index');
	Route::get('/{object}', 'ObjectsController@show');
	Route::get('/create', 'ObjectsController@create');
	Route::post('/', 'ObjectsController@store');
	Route::get('/{object}/edit', 'ObjectsController@edit');
	Route::patch('/{object}', 'ObjectsController@update');
	Route::delete('/{object}', 'ObjectsController@destroy');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'pages'], function(){
    //Route::get('/settings', 'PagesController@settings')->name('pages.settings');
    Route::get('/terms-and-conditions', 'PagesController@termsAndConditions')->name('pages.termsAndConditions');
});

Route::get('/logout', function(){
    Session::flush();
    Auth::logout();
    return Redirect::to("/login")
      ->with('message', array('type' => 'success', 'text' => 'You have successfully logged out'));
});

Route::get('vlayer', 'PagesController@showVlayer')->name('pages.vlayer');

Route::get('photo', 'PhotoController@index')->name('photo.index');
//--------------------------------------------------------------------------

//Eloquent CRUD

use App\User;
use App\Address;
use App\Post;
use App\Role;

// //one to one crud in address table

Route::get('address/insert', function(){
	$user = User::findOrFail(1);
	//dd($user);
	$address = new Address(['name'=>'1234 Huston NY']);
	//dd($user->address());
	$user->address()->save($address);//poate insera in mai multe coloane  
		echo "done";
});

Route::get('address/update', function(){
	$address = Address::whereUserId(1)->first();
	//dd($address);
	$address->name = '1234 Huston';
	$address->save();
	echo "done";
});

Route::get('address/read', function(){
	$user = User::findOrFail(1);
	//dd($user);
	echo $user->address->name;
	echo '</br>'."done";
});

Route::get('address/delete', function(){
	$user = User::findOrFail(1);
	//dd($user);
	/*echo*/ $user->address->delete();  //return 1
	echo '</br>'."done";
});

//-----------------------------------------------------
//one to many crud in posts table

Route::get('posts/insert', function(){
	$user = User::findOrFail(2);
	// $posts = new Post(['title'=>'My second post', 'body' => 'I love Laravel']);
	// $user->Posts()->save($posts);//poate insera in mai multe coloane  
	$user->Posts()->save(new Post(['title'=>'My third post', 'body' => 'I love Laravel']));
		echo "done";
});

Route::get('posts/read', function(){
	$user = User::findOrFail(1);
	//dd($user->Posts);   ///return colectie - diferite tipuri de obiecte, array
	//return $user->Posts;

	foreach($user->posts as $post){
		echo $post->title. '<br>';
	}
	echo '</br>'."done";
});

Route::get('posts/update', function(){
	$user = User::whereId(1)->first();
	$user->Posts()->whereId(1)->update(['title'=>'I love laravel', 'body' => 'I love laravel']);
	echo "done";
});

Route::get('posts/delete', function(){
	$user = User::find(1);
	//dd($user);
	/*echo*/ $user->Posts()->where('id',2)->delete();  //return 1
	echo '</br>'."done";
});

//---------------------------------------------------------
//many to many crud in roles table

Route::get('roles/create', function(){
	$user = User::find(2);
	$user->Roles()->save(new Role(['name'=>'author']));
		echo "done";
});

//in depth learning
Route::get('roles/read', function(){
	$user = User::findOrFail(1);
	foreach($user->Roles as $role){
		//dd($role);
		echo $role->name. '<br>';
	}
	echo '</br>'."done";
});

Route::get('roles/update', function(){
	$user = User::whereId(1)->first();
	// if($user->has('Roles'))
	// 	$user->Roles()->whereUserId(1)->update(['name'=>'Admin']);
	if($user->has('Roles'))
		foreach($user->roles as $role){
			if($role->name == 'Admin'){
				$role->name = strtolower($role->name);
				$role->save();
			}
		}
	echo "done";
});

Route::get('roles/delete', function(){
	$user = User::find(2);
	//dd($user);
	/*echo*/ $user->Roles()->whereUserId(2)->delete();  //return 1
	echo '</br>'."done";
});

Route::get('roles/attach', function(){
	$user = User::find(1);
	$user->Roles()->attach(2);  //return 1
	echo '</br>'."done";
});

Route::get('roles/detach', function(){
	$user = User::find(1);
	$user->Roles()->detach();  //return 1
	echo '</br>'."done";
});

Route::get('roles/sync', function(){
	$user = User::find(1); 
	$user->Roles()->sync([1, 2]);  //asociaza 1 la 1, daca gaseste
	echo '</br>'."done";
});


// Database - Eloquent
//Polymorphic Relationship CRUD

use App\Staff;
use App\Photo;

Route::get('polymorphic/create', function(){
	$saff = Staff::find(1);
	$saff->Photos()->create(['path' => 'another.jpg']);
	echo "done";
});

//in depth learning
Route::get('polymorphic/read', function(){
	$staff = Staff::findOrFail(1);
	foreach($staff->Photos as $photo){
		return $photo->path. '<br>';
	}
	echo '</br>'."done";
});

Route::get('polymorphic/update', function(){
	$staff = Staff::whereId(1)->first();
	$photo = $staff->Photos()->whereId(1)->first();
	$photo->path = 'update_example.jpg';
	$photo->save();
	echo "done";
});

Route::get('polymorphic/delete', function(){
	$staff = Staff::find(2);
	$staff->Photos->delete();  //not working
	echo '</br>'."done";
});

Route::get('polymorphic/assign', function(){
	$staff = Staff::find(1);
	$photo = Photo::find(5);

	$staff->Photos()->save($photo);  //not working
	echo '</br>'."done";
});

Route::get('polymorphic/un-assign', function(){
	$staff = Staff::find(1);

	$staff->Photos()->whereId(4)->update(['imageable_id' => 0, 'imageable_type' => ""]);  //not working
	echo '</br>'."done";
});


///--------------------------------------------------------------



